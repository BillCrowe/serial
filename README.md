

![SerialControlDebugPreview](https://bitbucket.org/BillCrowe/serial/raw/d177face0be7dc65e003f33c082e0de733fe7136/Screenshot.JPG)


Help File

Serial Debug v2.0

General Notes:
     The Serial Debug Text Controller component is intended for use as a testing and troubleshooting tool for controlling third party devices via a serial connection. The Debug Window will provide a console to view commands and responses.

Controls
     Debug Clear (Trigger)- Clears the entries in the Debug Window
     Debug Hex (Toggle)- Enables the Debug Window to display new commands and responses as hexadecimal notation
     Command[x] (String)- Command to be sent when the Send button is pressed
     Send[x] (Trigger)- Sends the corresponding Command string
     Notes[x] (String)- Text field to allow for quick notes regarding each command string
     Baud Rate (String)- Baud rate setting to be applied to the serial port
     Data Bits (String)- Sets the number of data bits for the serial port
     Parity (String)- Sets the parity bit setting for the serial port
     Stop Bits (String)- Sets the number of stop bits for the serial port
     
    
Application Notes
     *Hexadecimal notation follows the same rules as the Command Button component
          - Hexadecimal values should be preceded with \x which is case sensitive
          - Hexadecimal notation digits 00 - FF are not case sensitive (\xFF or \xff is acceptable)
          - Delimiter notations of \r (\x0d) and \n (\x0a) are case sensitive

KNOWN ISSUES
     None

Developer: Bill Crowe
Company: QSC

Release Notes:
Version v2.0
     Released Date: 2021.07.01
     Tested Q-Sys Firmware Version: 9.1.1
     Tested Hardware: Core 110f
     Notes: 
         - A Command Buttons component is no longer required for testing command strings
         - Dedicated Command String fields, Send buttons, and Notes fields have been added
Version v1.0 - Initial release
     Released Date: 2020.09.10
     Tested Q-Sys Firmware Version: 8.4.0
     Tested Hardware: Core 110f